package br.com.pitta.github;

import android.test.ActivityInstrumentationTestCase2;

import br.com.pitta.github.activity.MainActivity_;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static br.com.pitta.github.TesteUtil.actionOnItemViewAtPosition;
import static br.com.pitta.github.TesteUtil.withRecyclerView;

/**
 * Created by lucas_000 on 10/09/2016.
 * SIMPLE TEST CASE
 */
public class FunctionalTest extends ActivityInstrumentationTestCase2<MainActivity_>{

    public FunctionalTest() {
        super(MainActivity_.class);
    }

    @Override protected void setUp() throws Exception {
        getActivity();
    }

    public void testItemClick() {
        onView(withRecyclerView(R.id.list).atPosition(1)).check(matches(isDisplayed()));
        onView(withRecyclerView(R.id.list).atPosition(1)).perform(click());
    }

    public void testFollowButtonClick() throws InterruptedException {
        Thread.sleep(1500);
        onView(withId(R.id.list)).perform(actionOnItemViewAtPosition(1,
                R.id.row_repositories,
                click()));
        Thread.sleep(1500);
    }
}