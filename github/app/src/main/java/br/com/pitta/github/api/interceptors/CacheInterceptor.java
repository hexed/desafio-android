package br.com.pitta.github.api.interceptors;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;

import java.io.IOException;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class CacheInterceptor implements Interceptor {

    private static final int STALE = 60; // 1 minute
    private static final int AGE = 60 * 60; // 1 hour

    @Override
    public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        Request request = original.newBuilder()
                .header("Cache-Control", String.format("max-age=%d, max-stale=%d", AGE, STALE))
                .method(original.method(), original.body())
                .build();

        com.squareup.okhttp.Response response = chain.proceed(request);
        return response;
    }
}