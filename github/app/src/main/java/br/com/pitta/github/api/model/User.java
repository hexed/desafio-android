package br.com.pitta.github.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas_000 on 09/09/2016.
 */
public class User {

    @SerializedName("avatar_url")
    private String foto;
    public String getFoto() {
        return foto;
    }

    @SerializedName("login")
    private String nick;
    public String getNick() {
        return nick;
    }

    @Override
    public String toString() {
        return "User{" +
                "foto='" + foto + '\'' +
                ", nick='" + nick + '\'' +
                '}';
    }
}
