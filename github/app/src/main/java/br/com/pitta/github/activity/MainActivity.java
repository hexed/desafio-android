package br.com.pitta.github.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.pitta.github.R;
import br.com.pitta.github.api.model.Repositories;
import br.com.pitta.github.bus.BusProvider;
import br.com.pitta.github.event.GetRepositoriesEvent;
import br.com.pitta.github.event.SendErrorEvent;
import br.com.pitta.github.event.SendRepositoriesEvent;
import br.com.pitta.github.list.adapter.RepositoriesAdapter;
import br.com.pitta.github.list.listener.EndlessScrollListener;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    public static String DETAIL = "repositories";
    public static int page = 1;
    public static int last_page = 1;
    private Bus mBus = BusProvider.getInstance();
    private EndlessScrollListener endlessScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private RepositoriesAdapter adapter;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.list)
    RecyclerView list;

    @ViewById(R.id.list_progress)
    ProgressBar list_progress;

    @ViewById(R.id.progress)
    ProgressBar progress;

    @ViewById(R.id.empty_view)
    LinearLayout emptyView;

    @AfterViews
    void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main));

        adapter = new RepositoriesAdapter(this, new ArrayList<Repositories>());
        linearLayoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(linearLayoutManager);
        list.setAdapter(adapter);

        endlessScrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page = current_page;
                last_page = current_page;
                list_progress.setVisibility(View.VISIBLE);
                mBus.post(new GetRepositoriesEvent(current_page));
            }
        };
        list.addOnScrollListener(endlessScrollListener);

        page = 1;
        mBus.post(new GetRepositoriesEvent(page));
    }

    @Subscribe
    public void onSendRepositoriesEvent(SendRepositoriesEvent sendRepositoriesEvent) {
        List<Repositories> data = sendRepositoriesEvent.getRepositoriesList();

        if (data != null && data.size() > 0)
            adapter.addData(data);

        if(page < last_page) {
            mBus.post(new GetRepositoriesEvent(++page));
        } else {
            list_progress.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onSendErrorEvent(SendErrorEvent sendErrorEvent) {
        list_progress.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBus.unregister(this);
    }
}