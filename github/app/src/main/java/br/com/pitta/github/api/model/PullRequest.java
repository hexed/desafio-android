package br.com.pitta.github.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class PullRequest {

    @SerializedName("title")
    private String title;
    public String getTitle() {
        return title;
    }

    @SerializedName("body")
    private String descricao;
    public String getDescricao() {
        return descricao;
    }

    @SerializedName("user")
    private User usuario;
    public User getUsuario() {
        return usuario;
    }

    @SerializedName("html_url")
    private String url;
    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "PullRequest{" +
                "title='" + title + '\'' +
                ", descricao='" + descricao + '\'' +
                ", usuario=" + usuario +
                ", url='" + url + '\'' +
                '}';
    }
}
