package br.com.pitta.github.list.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.pitta.github.R;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class RepositoriesView extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView description;
    public TextView forks_count;
    public TextView stargazers_count;
    public ImageView foto;
    public TextView user;

    public RepositoriesView(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.titulo);
        description = (TextView) itemView.findViewById(R.id.descricao);
        forks_count = (TextView) itemView.findViewById(R.id.forks_count);
        stargazers_count = (TextView) itemView.findViewById(R.id.stargazers_count);
        foto = (ImageView) itemView.findViewById(R.id.foto);
        user = (TextView) itemView.findViewById(R.id.user);
    }
}