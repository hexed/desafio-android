package br.com.pitta.github.event;

import java.util.List;

import br.com.pitta.github.api.model.Repositories;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class SendRepositoriesEvent {

    List<Repositories> repositoriesList;

    public SendRepositoriesEvent(List<Repositories> repositoriesList) {
        this.repositoriesList = repositoriesList;
    }

    public List<Repositories> getRepositoriesList() {
        return repositoriesList;
    }
}
