package br.com.pitta.github.list.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.pitta.github.R;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class PullRequestsView extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView description;
    public ImageView foto;
    public TextView user;

    public PullRequestsView(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.titulo);
        description = (TextView) itemView.findViewById(R.id.descricao);
        foto = (ImageView) itemView.findViewById(R.id.foto);
        user = (TextView) itemView.findViewById(R.id.user);
    }
}