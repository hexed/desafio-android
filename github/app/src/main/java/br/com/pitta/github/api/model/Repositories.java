package br.com.pitta.github.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucas_000 on 09/09/2016.
 */
public class Repositories {

    @SerializedName("name")
    private String name;
    public String getName() {
        return name;
    }

    @SerializedName("description")
    private String descricao;
    public String getDescricao() {
        return descricao;
    }

    @SerializedName("forks_count")
    private int forks;
    public int getForks() {
        return forks;
    }

    @SerializedName("stargazers_count")
    private int stars;
    public int getStars() {
        return stars;
    }

    @SerializedName("owner")
    private User usuario;
    public User getUsuario() {
        return usuario;
    }

    @Override
    public String toString() {
        return "Repositories{" +
                "name='" + name + '\'' +
                ", descricao='" + descricao + '\'' +
                ", forks=" + forks +
                ", stars=" + stars +
                ", usuario=" + usuario +
                '}';
    }
}