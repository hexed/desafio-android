package br.com.pitta.github.event;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class GetRepositoriesEvent {

    private int page;

    public GetRepositoriesEvent(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }
}
