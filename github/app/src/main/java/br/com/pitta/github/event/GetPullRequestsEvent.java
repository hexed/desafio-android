package br.com.pitta.github.event;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class GetPullRequestsEvent {

    private String user;
    private String titulo;
    private int page;

    public GetPullRequestsEvent(String user, String titulo, int page) {
        this.user = user;
        this.titulo = titulo;
        this.page = page;
    }

    public String getUser() {
        return user;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getPage() {
        return page;
    }
}
