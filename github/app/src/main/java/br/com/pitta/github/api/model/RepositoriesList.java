package br.com.pitta.github.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lucas_000 on 09/09/2016.
 */
public class RepositoriesList {

    @SerializedName("items")
    private List<Repositories> repositoriesList;
    public List<Repositories> getRepositoriesList() {
        return repositoriesList;
    }
}