package br.com.pitta.github.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.pitta.github.R;
import br.com.pitta.github.api.model.PullRequest;
import br.com.pitta.github.api.model.Repositories;
import br.com.pitta.github.bus.BusProvider;
import br.com.pitta.github.event.GetPullRequestsEvent;
import br.com.pitta.github.event.SendErrorEvent;
import br.com.pitta.github.event.SendPullRequestEvent;
import br.com.pitta.github.list.adapter.PullRequestsAdapter;
import br.com.pitta.github.list.listener.EndlessScrollListener;

@EActivity(R.layout.activity_pull_requests)
public class PullRequestsActivity extends AppCompatActivity {

    public static int page = 1;
    public static int last_page = 1;
    private Bus mBus = BusProvider.getInstance();
    private EndlessScrollListener endlessScrollListener;
    private LinearLayoutManager linearLayoutManager;
    private PullRequestsAdapter adapter;
    private Repositories repositories;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.list)
    RecyclerView list;

    @ViewById(R.id.list_progress)
    ProgressBar list_progress;

    @ViewById(R.id.progress)
    ProgressBar progress;

    @ViewById(R.id.empty_view)
    LinearLayout emptyView;

    @AfterViews
    void init() {
        setSupportActionBar(toolbar);

        repositories = new Gson().fromJson(getIntent().getStringExtra(MainActivity.DETAIL), Repositories.class);
        if(repositories != null) {
            getSupportActionBar().setTitle(repositories.getName());

            adapter = new PullRequestsAdapter(this, new ArrayList<PullRequest>());
            linearLayoutManager = new LinearLayoutManager(this);
            list.setLayoutManager(linearLayoutManager);
            list.setAdapter(adapter);

            endlessScrollListener = new EndlessScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                    page = current_page;
                    last_page = current_page;
                    list_progress.setVisibility(View.VISIBLE);
                    mBus.post(new GetPullRequestsEvent(repositories.getUsuario().getNick(), repositories.getName(), current_page));
                }
            };
            list.addOnScrollListener(endlessScrollListener);

            page = 1;
            mBus.post(new GetPullRequestsEvent(repositories.getUsuario().getNick(), repositories.getName(), page));

        } else
            emptyView.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void onSendPullRequestsEvent(SendPullRequestEvent sendPullRequestsEvent) {
        List<PullRequest> data = sendPullRequestsEvent.getPullRequests();

        if (data != null && data.size() > 0)
            adapter.addData(data);

        if(page < last_page && repositories != null) {
            mBus.post(new GetPullRequestsEvent(repositories.getUsuario().getNick(), repositories.getName(), ++page));
        } else {
            list_progress.setVisibility(View.GONE);
            progress.setVisibility(View.GONE);
            emptyView.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onSendErrorEvent(SendErrorEvent sendErrorEvent) {
        list_progress.setVisibility(View.GONE);
        progress.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBus.unregister(this);
    }
}