package br.com.pitta.github.manager;

import android.content.Context;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import br.com.pitta.github.api.Client;
import br.com.pitta.github.api.model.PullRequest;
import br.com.pitta.github.api.model.RepositoriesList;
import br.com.pitta.github.event.GetPullRequestsEvent;
import br.com.pitta.github.event.GetRepositoriesEvent;
import br.com.pitta.github.event.SendErrorEvent;
import br.com.pitta.github.event.SendPullRequestEvent;
import br.com.pitta.github.event.SendRepositoriesEvent;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class Manager {

    private Context context;
    private Bus bus;
    private Client client;

    public Manager(Context context, Bus bus) {
        this.context = context;
        this.bus = bus;
        client = Client.getClient(context);
    }

    @Subscribe
    public void onGetRepositoriesEvent(GetRepositoriesEvent getRepositoriesEvent) {
        int page = getRepositoriesEvent.getPage();

        Callback<RepositoriesList> callback = new Callback<RepositoriesList>() {
            @Override
            public void success(RepositoriesList repositoriesList, Response response) {
                bus.post(new SendRepositoriesEvent(repositoriesList.getRepositoriesList()));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new SendErrorEvent());
            }
        };

        client.getRepositories(page, callback);
    }

    @Subscribe
    public void onGetPullRequestsEvent(GetPullRequestsEvent getPullRequestsEvent) {
        Callback<List<PullRequest>> callback = new Callback<List<PullRequest>>() {
            @Override
            public void success(List<PullRequest> pullRequests, Response response) {
                bus.post(new SendPullRequestEvent(pullRequests));
            }

            @Override
            public void failure(RetrofitError error) {
                bus.post(new SendErrorEvent());
            }
        };
        client.getPullRequests(getPullRequestsEvent.getUser(), getPullRequestsEvent.getTitulo(), getPullRequestsEvent.getPage(), callback);
    }
}