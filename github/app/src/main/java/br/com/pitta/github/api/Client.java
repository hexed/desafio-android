package br.com.pitta.github.api;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.util.List;

import br.com.pitta.github.api.interceptors.CacheInterceptor;
import br.com.pitta.github.api.interfaces.IGithub;
import br.com.pitta.github.api.model.PullRequest;
import br.com.pitta.github.api.model.RepositoriesList;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class Client {

    private static final String BASE_URL = "https://api.github.com";
    private static Client client;
    private static RestAdapter restAdapter;

    public static Client getClient(Context context) {
        if(client == null)
            client = new Client(context);

        return client;
    }

    private Client(Context context) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(new CacheInterceptor());

        try {
            long size = 10 * 1024 * 1024; //10mb
            File httpCacheDirectory = new File(context.getCacheDir(), "repositories");
            Cache cache = new Cache(httpCacheDirectory, size);
            okHttpClient.setCache(cache);
        } catch (Exception e) {
            e.printStackTrace();
        }

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    public void getRepositories(int page, Callback<RepositoriesList> callback) {
        String query = "language:Java";
        String sort = "stars";
        IGithub github = restAdapter.create(IGithub.class);
        github.getRepositories(query, sort, page, callback);
    }

    public void getPullRequests(String user, String titulo, int page, Callback<List<PullRequest>> callback) {
        IGithub github = restAdapter.create(IGithub.class);
        github.getPullRequests(user, titulo, page, callback);
    }
}