package br.com.pitta.github.list.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.pitta.github.R;
import br.com.pitta.github.activity.MainActivity;
import br.com.pitta.github.activity.PullRequestsActivity_;
import br.com.pitta.github.api.model.Repositories;
import br.com.pitta.github.list.view.RepositoriesView;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesView> {

    private List<Repositories> data;
    private Context context;

    public RepositoriesAdapter(Context context, List<Repositories> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RepositoriesView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_repositores, parent, false);
        RepositoriesView rv = new RepositoriesView(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(RepositoriesView holder, int position) {
        final Repositories repositories = data.get(position);

        holder.title.setText(repositories.getName());
        holder.description.setText(repositories.getDescricao());
        holder.forks_count.setText(Integer.toString(repositories.getForks()));
        holder.stargazers_count.setText(Integer.toString(repositories.getStars()));
        holder.user.setText(repositories.getUsuario().getNick());

        try {
            Picasso.with(context)
                    .load(repositories.getUsuario().getFoto())
                    .placeholder(R.drawable.ic_person_black_24dp)
                    .into(holder.foto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PullRequestsActivity_.class);
                intent.putExtra(MainActivity.DETAIL, new Gson().toJson(repositories));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void addData(List<Repositories> repositories) {
        if(this.data == null)
            this.data = new ArrayList<>();

        this.data.addAll(repositories);
        this.notifyDataSetChanged();
    }
}