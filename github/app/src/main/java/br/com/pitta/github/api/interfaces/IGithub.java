package br.com.pitta.github.api.interfaces;

import java.util.List;

import br.com.pitta.github.api.model.PullRequest;
import br.com.pitta.github.api.model.RepositoriesList;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public interface IGithub {

    @GET("/search/repositories")
    void getRepositories(@Query("q") String query, @Query("sort") String sort, @Query("page") int page, Callback<RepositoriesList> callback);

    @GET("/repos/{owner}/{repository}/pulls")
    void getPullRequests(@Path("owner") String user, @Path("repository") String titulo, @Query("page") int page, Callback<List<PullRequest>> callback);
}