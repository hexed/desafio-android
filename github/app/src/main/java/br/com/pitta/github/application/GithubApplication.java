package br.com.pitta.github.application;

import android.app.Application;

import com.squareup.otto.Bus;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import br.com.pitta.github.bus.BusProvider;
import br.com.pitta.github.manager.Manager;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class GithubApplication extends Application {

    private Manager manager;
    private Bus bus = BusProvider.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        manager = new Manager(this, bus);
        bus.register(manager);
        bus.register(this);

        /**
         * workaround to cache images better with picasso
         */
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }
}