package br.com.pitta.github.event;

import java.util.List;

import br.com.pitta.github.api.model.PullRequest;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class SendPullRequestEvent {

    List<PullRequest> pullRequests;

    public SendPullRequestEvent(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    public List<PullRequest> getPullRequests() {
        return pullRequests;
    }
}