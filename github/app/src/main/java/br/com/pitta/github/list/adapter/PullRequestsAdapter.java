package br.com.pitta.github.list.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.pitta.github.R;
import br.com.pitta.github.activity.MainActivity;
import br.com.pitta.github.activity.PullRequestsActivity_;
import br.com.pitta.github.api.model.PullRequest;
import br.com.pitta.github.api.model.Repositories;
import br.com.pitta.github.list.view.PullRequestsView;
import br.com.pitta.github.list.view.RepositoriesView;

/**
 * Created by lucas_000 on 10/09/2016.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsView> {

    private List<PullRequest> data;
    private Context context;

    public PullRequestsAdapter(Context context, List<PullRequest> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public PullRequestsView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pull_requests, parent, false);
        PullRequestsView prv = new PullRequestsView(v);
        return prv;
    }

    @Override
    public void onBindViewHolder(PullRequestsView holder, int position) {
        final PullRequest pullRequest = data.get(position);

        holder.title.setText(pullRequest.getTitle());
        holder.description.setText(pullRequest.getDescricao());
        holder.user.setText(pullRequest.getUsuario().getNick());

        try {
            Picasso.with(context)
                    .load(pullRequest.getUsuario().getFoto())
                    .placeholder(R.drawable.ic_person_black_24dp)
                    .into(holder.foto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent web = new Intent(Intent.ACTION_VIEW);
                web.setData(Uri.parse(pullRequest.getUrl()));
                context.startActivity(web);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void addData(List<PullRequest> pullRequests) {
        if(this.data == null)
            this.data = new ArrayList<>();

        this.data.addAll(pullRequests);
        this.notifyDataSetChanged();
    }
}